<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

#  Sentry Service
* [Service Overview](https://dashboards.gitlab.net/d/sentry-main/sentry-overview)
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22sentry%22%2C%20tier%3D%22inf%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:Sentry"

## Logging

* [system](https://log.gprd.gitlab.net/goto/b4618f79f80f44cb21a32623a275a0e6)

## Troubleshooting Pointers

* [../frontend/high-error-rate.md](../frontend/high-error-rate.md)
* [../gitaly/gitaly-down.md](../gitaly/gitaly-down.md)
* [../patroni/pg_xid_wraparound_alert.md](../patroni/pg_xid_wraparound_alert.md)
* [../patroni/postgresql-backups-wale-walg.md](../patroni/postgresql-backups-wale-walg.md)
* [../patroni/rotating-rails-postgresql-password.md](../patroni/rotating-rails-postgresql-password.md)
* [../praefect/praefect-startup.md](../praefect/praefect-startup.md)
* [../registry/migration-failure-scenarios.md](../registry/migration-failure-scenarios.md)
* [sentry-is-down.md](sentry-is-down.md)
* [../sidekiq/large-pull-mirror-queue.md](../sidekiq/large-pull-mirror-queue.md)
* [../sidekiq/sidekiq_error_rate_high.md](../sidekiq/sidekiq_error_rate_high.md)
* [../uncategorized/project-export.md](../uncategorized/project-export.md)
* [../web/static-objects-caching.md](../web/static-objects-caching.md)
<!-- END_MARKER -->

<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
